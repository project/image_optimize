<?php

/**
 * Retrieve information about the toolkit.
 */
function image_optimize_optipng_info() {
  return array(
    'name' => 'OptiPNG',
    'title' => t('OptiPNG is a PNG optimizer that recompresses image files to a smaller size, without losing any information.')
  );
}
