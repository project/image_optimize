<?php

/**
 * @file
 * Administrative pages for the image_optimize module.
 */

/**
 * Form callback.
 */
function image_optimize_settings() {
  $form['image_optimize'] = array(
    '#type' => 'textfield',
    '#title' => t('Image to optimize'),
    '#required' => TRUE,
    '#description' => t('Input image file path to optimize. It should be located in Drupal files directory.'),
  );

  $form['optimize'] = array(
    '#type' => 'submit',
    '#value' => t('Optimize image'),
  );

  //return system_settings_form($form);
  return $form;
}

/**
 * Form validate callback.
 */
function image_optimize_settings_validate($form, &$form_state) {
  // By default we allow files only in Drupal files directory.
//  $file_path = file_directory_path();

//  if (!file_create_path($form_state['values']['image_optimize'], $file_path)) {
  $file = &$form_state['values']['image_optimize'];

  if (!($file = file_create_path($file))) {
    form_set_error('image_optimize', t('Selected file does not exists or is not located in the files directory.'));
  }
}

/**
 * Form submit callback.
 */
function image_optimize_settings_submit($form, &$form_state) {
  image_optimize($form_state['values']['image_optimize']);
}
